def mean(numbers):
    if numbers == 0:
        return float('nan')
    else:
        total = sum(numbers)
        return  total / len(numbers)