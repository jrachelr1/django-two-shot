from django.urls import path

from receipts.views import (
    ReceiptListView, ReceiptCreateView, ExpenseCategoryListView, AccountListView, ExpenseCategoryCreateView, AccountCreateView
)

urlpatterns = [
    path('', ReceiptListView.as_view(), name='home'), #the empty string refers to the app name
    path('create/', ReceiptCreateView.as_view(), name='create_receipt'),
    path("categories/", ExpenseCategoryListView.as_view(), name="list_categories"),
    path("accounts/", AccountListView.as_view(), name="list_account"),
    path(
        "categories/create/",
        ExpenseCategoryCreateView.as_view(),
        name="create_categories",
    ),
    path(
        "accounts", #this is the path to the browser
        AccountCreateView.as_view(), #pulls in the model if it's a function view you write the function name
        name="create_account", #we give it this name - can use it in the view e.g. return redirect
    ),
]
